package br.com.pupposoft.poc.apache.camel.gateways.routers;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class KafkaToLogRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("kafka:test")
		.setProperty("originalMessage", body())
		.log("Data received from kafka: ${body}");
	}
}
