package br.com.pupposoft.poc.apache.camel.config;

import org.apache.camel.component.kafka.KafkaComponent;
import org.apache.camel.component.kafka.KafkaConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaConfig {

    @Bean(name = "kafka")
    public KafkaComponent createComponentKafka(@Value("${kafka.url}") String url) {
        KafkaComponent kafka = new KafkaComponent();
        KafkaConfiguration kafkaConfiguration = new KafkaConfiguration();
        kafkaConfiguration.setBrokers(url);
        kafka.setConfiguration(kafkaConfiguration);
        return kafka;
    }
    
}
