package br.com.pupposoft.poc.apache.camel.gateways.http.controllers.json;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class ResponseJson {
	private String name;
}
