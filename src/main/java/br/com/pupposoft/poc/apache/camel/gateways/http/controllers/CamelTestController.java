package br.com.pupposoft.poc.apache.camel.gateways.http.controllers;

import org.springframework.stereotype.Component;

import br.com.pupposoft.poc.apache.camel.gateways.http.controllers.json.RequestJson;
import br.com.pupposoft.poc.apache.camel.gateways.http.controllers.json.ResponseJson;

@Component
public class CamelTestController {

	public ResponseJson response(RequestJson requestJson) {
		
		//Add calls to useCase classes
		
		return new ResponseJson(requestJson.getName() + " (Changed on 'CamelTestController')");
	}
	
}
