package br.com.pupposoft.poc.apache.camel.gateways.routers;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class RestApiToKafkaRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		rest("/poc/api/send-kafka")
	        .consumes("application/json")
	        .produces("application/json")
			.post()
			.to("kafka:test");
		
	}
}
