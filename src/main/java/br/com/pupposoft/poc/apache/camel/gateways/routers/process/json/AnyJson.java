package br.com.pupposoft.poc.apache.camel.gateways.routers.process.json;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class AnyJson {
	private String name;
}
