package br.com.pupposoft.poc.apache.camel.gateways.routers;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

import br.com.pupposoft.poc.apache.camel.gateways.http.controllers.json.RequestJson;

@Component
public class RestApiRoute extends RouteBuilder {

	/*
	 *
	 * To access, use:
	 * 
	 curl --location --request POST 'http://localhost:8080/camel/poc/api/test' \
	--header 'Content-Type: application/json' \
	--data-raw '{
	    "name":"anyName"
	}'
	 * 		
	 */
	@Override
	public void configure() throws Exception {
		restConfiguration()
		.component("servlet")
		.bindingMode(RestBindingMode.auto);

		rest("/poc/api/test")
	        .consumes("application/json")
	        .produces("application/json")
			.post()
				.type(RequestJson.class)
				//.outType(ResponseJson.class)
			.to("bean:camelTestController");
	}
}
