package br.com.pupposoft.poc.apache.camel.gateways.routers;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class FileToFileRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("file:inputFolder").to("file:outputFolder");
	}

}
