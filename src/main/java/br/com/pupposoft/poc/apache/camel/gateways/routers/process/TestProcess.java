package br.com.pupposoft.poc.apache.camel.gateways.routers.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import br.com.pupposoft.poc.apache.camel.gateways.routers.process.json.AnyJson;

public class TestProcess implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		final AnyJson anyJson = exchange.getIn().getBody(AnyJson.class);
		
		final AnyJson anyJsonChanged = new AnyJson(anyJson.getName() + " (Value changed on 'TestProcess')");
		
		exchange.getIn().setBody(anyJsonChanged);
	}

}
