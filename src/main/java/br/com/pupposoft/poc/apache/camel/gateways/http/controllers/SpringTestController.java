package br.com.pupposoft.poc.apache.camel.gateways.http.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/poc/spring/api/test")
@CrossOrigin(origins = "*")
public class SpringTestController {

	@GetMapping
	public String test() {
		return "Spring MVC test: OK!";
	}
	
}
