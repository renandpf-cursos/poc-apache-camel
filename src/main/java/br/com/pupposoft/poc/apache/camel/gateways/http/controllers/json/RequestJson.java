package br.com.pupposoft.poc.apache.camel.gateways.http.controllers.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class RequestJson {
	private String name;
}
